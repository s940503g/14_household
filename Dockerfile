FROM nodered/node-red-docker:0.19.2-rpi-v8
USER root
RUN usermod -aG audio node-red
RUN apt update 
RUN apt install alsa-utils -y
RUN npm install node-red-dashboard@2.9.7 node-red-node-mongodb node-red-contrib-objectid
USER node-red
